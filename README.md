## tiare-user 8.1.0 OPM1.171019.026 V10.2.22.0.OCLMIXM release-keys
- Manufacturer: xiaomi
- Platform: 
- Codename: tiare
- Brand: Xiaomi
- Flavor: lineage_tiare-eng
- Release Version: 8.1.0
- Id: OPM7.181205.001
- Incremental: 41a193aca6
- Tags: test-keys
- CPU Abilist: armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Xiaomi/tiare/tiare:8.1.0/OPM1.171019.026/V10.2.22.0.OCLMIXM:user/release-keys
- OTA version: 
- Branch: tiare-user-8.1.0-OPM1.171019.026-V10.2.22.0.OCLMIXM-release-keys
- Repo: xiaomi_tiare_dump_22955


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
